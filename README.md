diff\_prune.vim
===============

This filetype plugin for diffs/patches ("diff" filetype) provides buffer-local
mappings in normal and visual mode to "undo" lines of changes defined by a
linewise motion or visual mode selection: leading minus signs are removed, and
lines with leading plus signs are deleted.

If the changes result in a diff block or file block having no changes left, it
is also removed.

This can be handy for using with the `-e` or `--edit` option to `git-add`,
which allows you to edit a diff before applying changes to the staging area.
It also seems to work for the `-p` or `--patch` option.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
